package ru.jsg.game.btb;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import ru.jsg.game.btb.handlers.ErrorHandler;
import ru.jsg.game.btb.handlers.KeyHandler;
import ru.jsg.game.btb.painting.PaintingQueue;

import java.nio.IntBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * Класс - ядро
 */
public final class Core {

    private static Core intense;

    private final long window; // id окна
    private final GLFWErrorCallback errorCallback; // обработчик ошибок
    private final KeyHandler keyCallback; // обработчик нажатий на клаву
    private final PaintingQueue queue;

    private final IntBuffer width;
    private final IntBuffer height;
    private int w;
    private int h;

    /**
     * Вызывается один раз призапуске приложения
     */
    private Core() {
        // если будут ошибки ядра то мы узнаем об этом в логах
        glfwSetErrorCallback(errorCallback = new ErrorHandler());

        // пытаемся инициализировать библиотеку
        // при невозможности это сделать выдаём ошибку
        if (glfwInit() != GLFW_TRUE) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }

        // Инициализируем обработчик очереди отрисовки
        queue = new PaintingQueue(this);

        // создаём окно
        // если это сделать не получается -- выдаём ошибку
        window = glfwCreateWindow(Const.WEIGH, Const.HEIGHT, Const.TITLE, NULL, NULL);
        if (window == NULL) {
            glfwTerminate();
            throw new RuntimeException("Failed to create the GLFW window");
        }

        // обработчик нажатия клавиш
        keyCallback = new KeyHandler();
        glfwSetKeyCallback(window, keyCallback); // привязваем его к окну

        // делаем окно по центру
        GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(window,
                (vidMode.width() - Const.WEIGH) / 2,
                (vidMode.height() - Const.HEIGHT) / 2
        );

        // создаём GL контекст в окне
        glfwMakeContextCurrent(window);
        GL.createCapabilities();

        // вертикальная синхронизация
        glfwSwapInterval(1);

        // создадим пустые значения в памяти
        height = BufferUtils.createIntBuffer(2);
        width = BufferUtils.createIntBuffer(2);
    }

    public static Core init() {
        return intense = new Core();
    }

    public static Core getIntense() {
        if (intense == null) init();
        return intense;
    }

    /**
     * Вызывается при каждой отрисовке экрана
     *
     * @param time - время в секундах после старта(после вызова glfwInit)
     */
    public void loop(double time) {
        float ratio; // соотношение сторон

        // получаем ширину и высоту и считаем соотношение сторон
        glfwGetFramebufferSize(window, width, height);
        ratio = (w = width.get()) / (float) (h = height.get());

        // очистим для следущей интерации
        width.rewind();
        height.rewind();

        // создадим поле
        glViewport(0, 0, width.get(), height.get());
        glClear(GL_COLOR_BUFFER_BIT);

        // Отрисовываем всё
        queue.paintAll(time, ratio, h, w);

        // Swap buffers and poll Events
        glfwSwapBuffers(window);
        glfwPollEvents();

        // Flip buffers for next loop
        width.flip();
        height.flip();
    }

    public void startLoop() {
        while (glfwWindowShouldClose(window) != GLFW_TRUE) {
            loop(glfwGetTime());
        }
        onEnd();
    }

    private void onEnd() {
        // Release window and its callbacks
        glfwDestroyWindow(window);
        keyCallback.release();

        // Terminate GLFW and release the error callback
        glfwTerminate();
        errorCallback.release();
    }

    /**
     * Делает окно видимым
     */
    public void show() {
        glfwShowWindow(window);
    }

    public KeyHandler getKeyCallback() {
        return keyCallback;
    }

    public PaintingQueue getPaintingQueue() {
        return queue;
    }
}
