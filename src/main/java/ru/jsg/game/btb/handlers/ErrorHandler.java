package ru.jsg.game.btb.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;

import java.util.Map;

import static org.lwjgl.system.APIUtil.apiClassTokens;
import static org.lwjgl.system.MemoryUtil.memDecodeUTF8;

public class ErrorHandler extends GLFWErrorCallback {

    private static final Logger log = LogManager.getLogger("[LWJGL ERROR]");

    private final Map<Integer, String> ERROR_CODES = apiClassTokens((field, value) -> 0x10000 < value && value < 0x20000, null, GLFW.class);

    @Override
    public void invoke(int error, long description) {
        String msg = memDecodeUTF8(description);

        log.error(ERROR_CODES.get(error) + " error");
        log.error("Description : " + msg);
        log.error("Stacktrace  :");
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        for (int i = 4; i < stack.length; i++) {
            log.error("    " + stack[i].toString());
        }
    }
}
