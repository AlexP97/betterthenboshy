package ru.jsg.game.btb.handlers;

@FunctionalInterface
public interface KeyListener {
    /**
     * @param window id окна (фрейма)
     * @param key    id клавиши
     * @param action нажата(1) или нет(0) или её держат нажатой(2)
     * @param mods   нажаты ли такие клавиши как Ctrl, Alt итп
     */
    void onKeyPerformed(long window, int key, int scancode, int action, int mods);
}
