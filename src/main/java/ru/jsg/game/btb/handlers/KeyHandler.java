package ru.jsg.game.btb.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFWKeyCallback;

import java.util.ArrayList;

public class KeyHandler extends GLFWKeyCallback {

    private static final Logger log = LogManager.getLogger(KeyHandler.class);

    private ArrayList<KeyListener> listeners = new ArrayList<>();

    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) {
        listeners.forEach(listener -> listener.onKeyPerformed(window, key, scancode, action, mods));
    }

    /**
     * Добавляет слушатель клавиш
     *
     * @param l - слушатель клавиш
     */
    public void addKeyListerner(KeyListener l) {
        listeners.add(l);
    }

}
