package ru.jsg.game.btb.samples;

import org.lwjgl.opengl.GL11;
import ru.jsg.game.btb.Core;
import ru.jsg.game.btb.painting.Printable;

public class Triangle extends Printable {

    private final float rotationState;

    public Triangle(int zindex, float rotationState) {
        setZIndex(zindex);
        this.rotationState = rotationState;
    }


    @Override
    public void print(double time, Core core, float ratio, int width, int height) {
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(-ratio, ratio, -1f, 1f, 1f, -1f);
        GL11.glRotatef((float) time * 50f * rotationState, 0f, 0f, 1f);

        // рисуем треугольник
        GL11.glBegin(GL11.GL_TRIANGLES);
        GL11.glColor3f(1f, 0f, 0f);
        GL11.glVertex3f(-0.6f, -0.4f, 0f);
        GL11.glColor3f(0f, 1f, 0f);
        GL11.glVertex3f(0.6f, -0.4f, 0f);
        GL11.glColor3f(0f, 0f, 1f);
        GL11.glVertex3f(0f, 0.6f, 0f);
        GL11.glEnd();
    }
}
