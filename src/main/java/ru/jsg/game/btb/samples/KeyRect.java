package ru.jsg.game.btb.samples;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;
import ru.jsg.game.btb.BetterThenBoshy;
import ru.jsg.game.btb.Const;
import ru.jsg.game.btb.Core;
import ru.jsg.game.btb.handlers.KeyListener;
import ru.jsg.game.btb.painting.Printable;

public class KeyRect extends Printable implements KeyListener {

    public static final Logger log = LogManager.getLogger(KeyRect.class);

    private int[] position = new int[]{0, 0};
    private int[] move = new int[]{0, 0};

    public KeyRect() {
        setZIndex(Printable.PLAYER_ZINDEX); // типа это игрок
        BetterThenBoshy.getCore().getKeyCallback().addKeyListerner(this); // добавим слушателя кнопок
        // да, эта длинная запись мне тоже не нравится. потом что-нибудь с ней сделаю
    }

    @Override
    public void print(double time, Core core, float ratio, int width, int height) {
        position[0] += move[0] * 2;
        position[1] += move[1] * 2;


        GL11.glLoadIdentity();
        GL11.glOrtho(0, Const.WEIGH, Const.HEIGHT, 0, 1, -1);

        GL11.glColor3f(0.5f, 0.5f, 1.0f);
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glVertex2f(position[1], position[0]);
        GL11.glVertex2f(position[1] + 100, position[0]);
        GL11.glVertex2f(position[1] + 100, position[0] + 100);
        GL11.glVertex2f(position[1], position[0] + 100);
        GL11.glEnd();
    }

    @Override
    public void onKeyPerformed(long window, int key, int scancode, int action, int mods) {

        switch (key) {
            case GLFW.GLFW_KEY_UP:
                move[0] = (action != GLFW.GLFW_RELEASE) ? -1 : 0;
                break;
            case GLFW.GLFW_KEY_DOWN:
                move[0] = (action != GLFW.GLFW_RELEASE) ? 1 : 0;
                break;
            case GLFW.GLFW_KEY_LEFT:
                move[1] = (action != GLFW.GLFW_RELEASE) ? -1 : 0;
                break;
            case GLFW.GLFW_KEY_RIGHT:
                move[1] = (action != GLFW.GLFW_RELEASE) ? 1 : 0;
                break;
        }
    }
}
