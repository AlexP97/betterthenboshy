package ru.jsg.game.btb;


import ru.jsg.game.btb.painting.Printable;
import ru.jsg.game.btb.samples.KeyRect;
import ru.jsg.game.btb.samples.Triangle;

public class BetterThenBoshy {

    private static Core core;

    public static void main(String[] args) {
        core = Core.init(); // иницализируем ядро

        // example
        new KeyRect().register();
        new Triangle(Printable.BACKGROUND_ZINDEX, 1f).register();
        new Triangle(Printable.FOREGROING_ELEMENTS_ZINDEX, -0.3f).register();

        core.show(); // показываем окно
        core.startLoop(); // запуск цикла отрисовки
    }

    public static Core getCore() {
        return core;
    }
}
