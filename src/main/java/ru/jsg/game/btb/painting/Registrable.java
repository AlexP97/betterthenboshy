package ru.jsg.game.btb.painting;

public interface Registrable {
    void register();
    void remove();
}
