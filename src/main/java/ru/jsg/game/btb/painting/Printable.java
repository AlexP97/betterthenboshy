package ru.jsg.game.btb.painting;

import ru.jsg.game.btb.BetterThenBoshy;
import ru.jsg.game.btb.Core;

/**
 * Элемент который можно отрисовать в цикле отрисовки
 */
public abstract class Printable implements Registrable {

    public static final int BACKGROUND_ZINDEX = -10; // задний фон
    public static final int FOREGROUND_ZINDEX = -5; // декорации (облака итп)
    public static final int ELEMENTS_ZINDEX = 0; // элементы обстановки (здания деревья итп)
    public static final int PLAYER_ZINDEX = 20; // сам игрок
    public static final int FOREGROING_ELEMENTS_ZINDEX = 21; // всё что перед игроком

    public static final int FULL_FRONT_ZINDEX = Short.MAX_VALUE;
    public static final int FULL_BACK_ZINDEX = Short.MIN_VALUE;

    private final Core core;
    private int zindex = 1;

    public Printable(Core core) {
        this.core = core;
    }

    public Printable() {
        this(BetterThenBoshy.getCore());
    }

    /**
     * Вызывается каждый раз при отрисовки элемента на экране
     *
     * @param time время с начала запуска
     */
    public abstract void print(double time, Core core, float ratio, int width, int height);

    /**
     * @return индекс отрисовки элемента на экране
     */
    public int getZIndex() {
        return zindex;
    }

    /**
     * Изменяет значение индекса отрисовки элемента
     *
     * @param index z-index
     */
    public void setZIndex(int index) {
        zindex = index;
        core.getPaintingQueue().sortNeeded();
    }

    @Override
    public void register() {
        core.getPaintingQueue().add(this);
    }

    @Override
    public void remove() {
        core.getPaintingQueue().remove(this);
    }
}
