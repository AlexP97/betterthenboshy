package ru.jsg.game.btb.painting;

import ru.jsg.game.btb.Core;

import java.util.ArrayList;

/**
 * Организует отрисовку элементов на экране
 */
public class PaintingQueue {

    private final ArrayList<Printable> queue;
    private final Core core;
    private boolean isSorted = true;

    public PaintingQueue(Core core) {
        queue = new ArrayList<>();
        this.core = core;
    }

    /**
     * Отрисовывает все элементы
     */
    public void paintAll(double time, float ratio, int width, int height) {
        if (!isSorted) sort();
        queue.forEach(printable -> printable.print(time, core, ratio, width, height));
    }

    public void sort() {
        for (int i = queue.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
            /*
                Сравниваем элементы попарно,
                если они имеют неправильный порядок,
                то меняем местами
             */
                if (queue.get(j).getZIndex() > queue.get(j + 1).getZIndex()) {
                    Printable tmp = queue.get(j);
                    queue.set(j, queue.get(j + 1));
                    queue.set(j + 1, tmp);
                }
            }
        }
        isSorted = true;
    }

    public void add(Printable p) {
        queue.add(p);
        sortNeeded();
    }

    public void remove(Printable p) {
        queue.remove(p);
    }

    public void sortNeeded() {
        isSorted = false;
    }
}
